﻿using UnityEngine;

public class InformerScript : MonoBehaviour
{
    public int bobic_count;
    [SerializeField] private int specService;
    [SerializeField] private int secretAgents;
    public double budget = 0;
    public string key = "";
    ObjectsScript Objects;
    public string code_word;
    public GameObject choosenPoint;
    public int police_count;
    public int emoji_count;
    public int houses_count = 5;
    public int humans_count = 1;
    public float button_Width;
    public float button_Height;
    public Vector2 mouse_Pos;
    public bool active_bottons = false;
    public bool active_bottons2 = false;
    public bool isMouseButtonLeft = false;
    public bool isMouseButtonRight = false;
    public bool isMouseButtonMiddle = false;
    public bool isMouseScrollWheelDown = false;
    public bool isMouseScrollWheelUp = false;
    public SystemOfTime time;
    public double Reputation = 0;
    public bool  pause = false;
    public int crowd_count;

    [System.Serializable]
    public struct coefficients
    {
        public const double k1 = 1.5; // destroying of building
        public const double k2 = 0.75; // destroying of roads
        public const double k3 = 1.2; // restoring of buildings
        public const double k4 = 0.5; // restoring of roads
        public const double k5 = 2.0; // first type holidays
        public const double k6 = 1.5; //second type holidays
        public const double k7 = 1.2; // third type holidays
        public const double k8 = 2;   // donations for disabled people and etc
        public const double k9 = 4.5;   // destroying of building(neighboring)
        public const double k10 = 2.5;   // restoring of building(neighboring)
    }

    public float cam_Width;
    public float cam_Height;
    public float cam_Top;
    public float cam_Bottom;
    public float cam_Right;
    public float cam_Left;
    public int discrete;

    void Start()
    {
        discrete = 100;
        code_word = "";
        police_count = 25;
        emoji_count = 0;
        Objects = GameObject.Find("Objecter").GetComponent<ObjectsScript>();
        button_Width = Objects.button.GetComponent<BoxCollider2D>().size.x;
        button_Height = Objects.button.GetComponent<BoxCollider2D>().size.y;
        cam_Height = 2f * Objects.main_cam.orthographicSize;
        cam_Width = cam_Height * Objects.main_cam.aspect;
        cam_Top = cam_Height / 2;
        cam_Bottom = -cam_Height / 2;
        cam_Right = cam_Width / 2;
        cam_Left = -cam_Width / 2;
        choosenPoint = new GameObject();
        choosenPoint = Objects.points[Random.Range(0, Objects.points.Length - 1)];
    }
    void Update()
    {
        if (Reputation > 100.0)
        {
            Reputation = 100.0;
        }
        if (Reputation < -100.0)
        {
            Reputation = -100.0;
        }
        time = GameObject.Find("city-map").GetComponent<SystemOfTime>();
        isMouseButtonLeft = Input.GetMouseButtonDown(0);
        isMouseButtonRight = Input.GetMouseButtonDown(2);
        isMouseButtonMiddle = Input.GetMouseButtonDown(1);
        isMouseScrollWheelDown = Input.GetAxis("Mouse ScrollWheel") < -0.1;
        isMouseScrollWheelUp = Input.GetAxis("Mouse ScrollWheel") > 0.1;
        Vector3 pos = Input.mousePosition;
        Ray mouseRay = Objects.main_cam.ScreenPointToRay(pos);
        mouse_Pos = mouseRay.GetPoint(1f);
        //FindObjectOfType<AudioManager>().Play("Beep");
    }
    public int GetSecretAgents()
    {
        return secretAgents;
    }
    public void SubSecretAgents(int amount)
    {
        secretAgents -= amount;
    }
    public void AddSecretAgents(int amount)
    {
        secretAgents += amount;
    }
    public int GetSpecService()
    {
        return specService;
    }
    public void SubSpecService(int amount)
    {
        specService -= amount;
    }
    public void AddSpecService(int amount)
    {
        specService += amount;
    }
}
