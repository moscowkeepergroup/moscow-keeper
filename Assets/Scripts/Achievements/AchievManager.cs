﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AchievManager : MonoBehaviour
{
    private List<Achiev> _achievs;

    private bool _isCompleted;

    private List<Achiev> _receivedAchives;

    public InformerScript info;

    public InfoDate date;

    private Text _text;

    public float shiftTime;

    private Achiev CreateAchieve(string descript, params string[] param)
    {
        List<string> curList = new List<string>();
        List<int> targetList = new List<int>();
        int half = param.Length / 2;
        for (int i = 0; i < half; i++)
        {
            curList.Add(param[i]);
            int parseInt;
            if (int.TryParse(param[i + half], out parseInt))
                targetList.Add(parseInt);
            else
                return null;
        }
        var achiev = new Achiev(descript, curList, targetList);
        achiev.SetDescription(descript);
        achiev.SetCurrentValues(curList);
        achiev.SetTargetValues(targetList);
        return achiev;
    }


    // Start is called before the first frame update
    void Start()
    {
        _text = GetComponent<Text>();
        _achievs = new List<Achiev>();
        _receivedAchives = new List<Achiev>();
        _achievs.Add(CreateAchieve("Собрать 200р",
            "money", "200"));
        _achievs.Add(CreateAchieve("Собрать 30 полицейских",
            "police", "30"));
        _achievs.Add(CreateAchieve("Собрать 300р и 40 полицейских",
            "money", "police", "300", "40"));
        _achievs.Add(CreateAchieve("Продержаться 25 дней", "day", "25"));
        _achievs.Add(CreateAchieve("Продержаться 1 месяц", "month", "1"));
        _isCompleted = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_achievs.Count == 0 || _isCompleted) return; //Проверяет что цепочка квестов не пустая
        for (int i = 0; i < _achievs.Count; i++)
        {
            if (CheckComplete(_achievs[i]))
            {
                _isCompleted = true;
                _text.text = ShowStats(_achievs[i]);
                StartCoroutine(DownShift(i));
                Debug.Log("Achiement complete!!!");
            }
        }
    }

    private int GetNum(string str)
    {
        if (str.ToLower().Equals("money"))
            return (int)info.budget;
        if (str.ToLower().Equals("police"))
            return info.police_count;
        if (str.ToLower().Equals("day"))
            return (int)date.days;
        if (str.ToLower().Equals("month"))
            return (int)date.months + ((int)date.years - 2019) * 12 - 8;
        if (str.ToLower().Equals("year"))
            return (int)date.years - 2019;
        return 0;
    }

    //Создает строку для вывода в UI
    public string ShowStats(Achiev achieve)
    {
        var res = "Достижение получено:\n" + achieve.GetDescription();
        return res;
    }

    //Проверяет текущий квест на выполнение
    public bool CheckComplete(Achiev achieve)
    {
        List<String> currentValues = achieve.GetCurrentValues();
        List<int> targetValues = achieve.GetTargetValues();

        for (int i = 0; i < currentValues.Count; i++)
        {
            if (GetNum(currentValues.ElementAt(i)) < targetValues.ElementAt(i))
                return false;
        }
        return true;
    }

    private IEnumerator DownShift(int index)
    {
        GameObject parentGameObject = transform.parent.gameObject;
        Vector3 parentPosition = parentGameObject.transform.position;

        float x = parentPosition.x;
        float y = parentPosition.y;
        float z = parentPosition.z;
        for (float i = 0; i < 10; i++)
        {
            parentGameObject.transform.position = new Vector3(x, y - i / 2, z);
            yield return new WaitForSeconds(shiftTime / 20);
        }
        ReceivedAchieve(index);
    }

    private IEnumerator UpShift()
    {
        yield return StartCoroutine(Waiting(3.0f));
        GameObject parentGameObject = transform.parent.gameObject;
        Vector3 parentPosition = parentGameObject.transform.position;

        float x = parentPosition.x;
        float y = parentPosition.y;
        float z = parentPosition.z;
        for (float i = 0; i < 10; i++)
        {
            parentGameObject.transform.position = new Vector3(x, y + i / 2, z);
            yield return new WaitForSeconds(shiftTime / 20);
        }
        _isCompleted = false;
    }

    private IEnumerator Waiting(float time)
    {
        yield return new WaitForSeconds(time);
    }

    private void ReceivedAchieve(int index)
    {
        _receivedAchives.Add(_achievs[index]);
        _achievs.RemoveAt(index);
        StartCoroutine(UpShift());
    }
}
