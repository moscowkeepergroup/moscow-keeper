﻿using System.Collections.Generic;

public class Achiev
{
    private string _description;

    private List<string> _currentValues;

    private List<int> _targetValues;

    private bool _isComplete;

    public Achiev(string description, List<string> currentValues, List<int> targetValues)
    {
        _description = description;
        _currentValues = currentValues;
        _targetValues = targetValues;
    }

    public void SetDescription(string description)
    {
        _description = description;
    }
    public string GetDescription()
    {
        return _description;
    }

    public void SetTargetValues(List<int> targetValues)
    {
        _targetValues = targetValues;
    }
    public List<int> GetTargetValues()
    {
        return _targetValues;
    }

    public void SetCurrentValues(List<string> currentValues)
    {
        _currentValues = currentValues;
    }
    public List<string> GetCurrentValues()
    {
        return _currentValues;
    }
}
