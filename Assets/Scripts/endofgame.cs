﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class endofgame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameObject.Find("interface").GetComponent<gamepause>().bb)
            StartCoroutine(wait());
    }
    IEnumerator wait()
    {
        yield return new WaitForSeconds(7f);
        SceneManager.LoadScene(0);
    }
}
