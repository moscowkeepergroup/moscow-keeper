﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class CameraControl2 : MonoBehaviour
{
    private Vector3 StartPosition;
    float posx, posy;
    Vector3 touchStart;
    public float zoomOutMin = 2;
    public float zoomOutMax = 14;
    void Update()
    {
        var fingerCount = 0;
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
            {
                fingerCount++;
            }
        }
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);

            // Handle finger movements based on TouchPhase
            switch (touch.phase)
            {
                //When a touch has first been detected, change the message and record the starting position
                case TouchPhase.Began:
                    StartPosition = touch.position;
                    break;

                //Determine if the touch is a moving touch
                case TouchPhase.Moved:
                    posx = touch.position.x - StartPosition.x;
                    posy = touch.position.y - StartPosition.y;
                    transform.position = new Vector3(Mathf.Clamp(transform.position.x - posx, -10.0f, 10.0f), Mathf.Clamp(transform.position.y - posy, -10.0f, 10.0f), transform.position.z);
                    break;

                case TouchPhase.Ended:
                    break;
            }
        }


        if (Input.GetMouseButtonDown(0))
        {
            touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

            float difference = currentMagnitude - prevMagnitude;

            zoom(difference * 0.02f);
        }
        else if (Input.GetMouseButton(0))
        {
            Vector3 direction = touchStart - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Camera.main.transform.position += direction;
        }
        zoom(Input.GetAxis("Mouse ScrollWheel"));
    }

    void zoom(float increment)
    {
        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - increment, zoomOutMin, zoomOutMax);
    }
}


