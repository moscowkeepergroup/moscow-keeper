﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    private Vector3 StartPosition;
    private Camera cam;
    void Start()
    {
        cam = GetComponent<Camera>();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            StartPosition = cam.ScreenToWorldPoint(Input.mousePosition);
        }
        else if (Input.GetMouseButton(1))
        {

            float posx = cam.ScreenToWorldPoint(Input.mousePosition).x - StartPosition.x;
            float posy = cam.ScreenToWorldPoint(Input.mousePosition).y - StartPosition.y;

            transform.position = new Vector3(Mathf.Clamp(transform.position.x - posx, -10.0f, 10.0f), Mathf.Clamp((transform.position.y - posy), -10.0f, 10.0f), transform.position.z);

        }
    }
}
