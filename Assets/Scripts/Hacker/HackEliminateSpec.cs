﻿using UnityEngine;

public class HackEliminateSpec : MonoBehaviour
{
    [SerializeField] private int eliminateCost;

    private InformerScript _info;

    private GameObject _parent;

    public void OnClick()
    {
        if (_info.GetSpecService() < eliminateCost) return;

        FindObjectOfType<AudioManager>().Play("Punch");
        _info.SubSpecService(eliminateCost);
        Destroy(_parent);
        Destroy(_parent.transform.parent.gameObject);
    }
    void Start()
    {
        _info = GameObject.Find("Informer").GetComponent<InformerScript>();
        _parent = transform.parent.gameObject;
    }
}
