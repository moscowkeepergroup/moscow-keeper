﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HackEliminatePolice : MonoBehaviour
{
    [SerializeField] private int eliminateCost;

    private InformerScript _info;

    private GameObject _parent;

    public void OnClick()
    {
        if (_info.police_count < eliminateCost) return;

        FindObjectOfType<AudioManager>().Play("Punch");
        _info.police_count -= eliminateCost;
        Destroy(_parent);
        Destroy(_parent.transform.parent.gameObject);
    }
    void Start()
    {
        _info = GameObject.Find("Informer").GetComponent<InformerScript>();
        _parent = transform.parent.gameObject;
    }
}
