﻿using UnityEngine;

public class HackerPrefabController : MonoBehaviour
{
    //bool ok = false;
    public GameObject Menu;
    //GameObject O;

    private HackersManager _hackersManager;

    private void Start()
    {
        _hackersManager = GameObject.Find("HackerPrefab").gameObject.GetComponent<HackersManager>();
    }

    public void OnClick()
    {
        if (_hackersManager.GetHackerPanelExist())
        {
            var temp = GameObject.FindWithTag("HackerPanel");
            Debug.Log(temp);
            Destroy(temp);
        }
        FindObjectOfType<AudioManager>().Play("Keyboard");
        Instantiate(Menu, transform.position, Quaternion.identity, transform);
        _hackersManager.SetHackerPanelExist(true);
    }

}
