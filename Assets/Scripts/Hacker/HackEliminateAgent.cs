﻿using UnityEngine;

public class HackEliminateAgent : MonoBehaviour
{
    [SerializeField] private int eliminateCost;

    private InformerScript _info;

    private GameObject _parent;

    public void OnClick()
    {
        if (_info.GetSecretAgents() < eliminateCost) return;

        FindObjectOfType<AudioManager>().Play("Punch");
        _info.SubSecretAgents(eliminateCost);
        Destroy(_parent);
        Destroy(_parent.transform.parent.gameObject);
    }
    void Start()
    {
        _info = GameObject.Find("Informer").GetComponent<InformerScript>();
        _parent = transform.parent.gameObject;
    }
}
