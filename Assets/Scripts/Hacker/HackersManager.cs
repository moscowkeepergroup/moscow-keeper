﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class HackersManager : MonoBehaviour
{
    [SerializeField] private GameObject houses;

    [SerializeField] private float deviate;
    
    private Transform _housesTransform;

    private bool _hackerPanelExist;

    [SerializeField] private GameObject prefab;

    [SerializeField] private InformerScript _informerScript;

    [SerializeField] private float _timeForRetry;
    
    private Random _random;

    private int _childCount;

    private bool _isActive;
    
    void Start()
    {
        _housesTransform = houses.transform;
        _childCount = houses.transform.childCount;
        _hackerPanelExist = false;
        _isActive = true;
        StartCoroutine(ChangeActive());
    }

    public bool GetHackerPanelExist()
    {
        return _hackerPanelExist;
    }

    public void SetHackerPanelExist(bool hackerPanelExist)
    {
        _hackerPanelExist = hackerPanelExist;
    }

    void Update()
    // Update is called once per frame
    {
        if (_isActive) return;
        
        double proba = (_informerScript.Reputation + 100) / 200;
        float randomValue = Gaussian.NextGaussian(0, deviate);
        _isActive = true;
        StartCoroutine(ChangeActive());
        if (randomValue < 0 || randomValue > 1) return;
        
        if (randomValue < proba) return;
        Debug.Log(randomValue + " and rep is " + proba);
        int houseNum = Random.Range(0, _childCount - 1);
        Transform house = _housesTransform.GetChild(houseNum);
        for (int i = 0; i < house.childCount; i++)
            if (house.GetChild(i).name.Equals("hacker(Clone)")) return;
        Instantiate(prefab, _housesTransform.GetChild(houseNum).transform.position, 
            new Quaternion(0, 0, 0, 0), _housesTransform.GetChild(houseNum));
    }

    private IEnumerator ChangeActive()
    {
        yield return new WaitForSeconds(_timeForRetry);
        _isActive = false;
    }
}
