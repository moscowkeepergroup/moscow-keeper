﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kinematic : MonoBehaviour

{
    float time;
    int[] i = new int[3];
    int[] j = new int[3];

    private void Start()
    {
        i[1] = Random.Range(-100, 100);
        i[0] = Random.Range(-100, 100);
        i[2] = Random.Range(-100, 100);
        j[0] = Random.Range(-100, 100);
        j[2] = Random.Range(-100, 100);
        j[1] = Random.Range(-100, 100);

    }
    void Update()
    {
        /*  time += Time.deltaTime;
          if (time <= 1)
          {
              transform.position += ((new Vector3(i[1],i[2],i[0]) - this.transform.position).normalized * time * 2 - (new Vector3(j[0],j[1],j[2])).normalized* time * time / 2)/16 ;
          }
          else
          {
              transform.position += (GameObject.Find("money").GetComponent<RectTransform>().position - this.
                          transform.position).normalized * Time.deltaTime * 250;
              if ((this.transform.position - GameObject.Find("money").GetComponent<RectTransform>().position).magnitude <2)
                  Destroy(gameObject);
          }*/

    }
    private void FixedUpdate()
    {

        if (time <= 1)
        {
            transform.position += ((new Vector3(i[1], i[2], i[0]) - this.transform.position).normalized * time * 2 - (new Vector3(j[0], j[1], j[2])).normalized * time * time / 2) / 16;
            FindObjectOfType<AudioManager>().Play("Coin");
        }
        else
        {
            transform.position += (GameObject.Find("money").GetComponent<RectTransform>().position - this.
                        transform.position).normalized * 3;

            if ((this.transform.position - GameObject.Find("money").GetComponent<RectTransform>().position).magnitude < 2)
            {
                Destroy(gameObject);
            }
        }
        time += 0.02f;
    }

}
