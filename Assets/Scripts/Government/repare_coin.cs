﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class repare_coin : MonoBehaviour

{
    float time;
    float time2;
    public float s;
    bool move = false;
    int n=15;
    public GameObject coin2;




    public void Update()
    {

        if (move && time - time2 > 0.05 && n >= 1)
        {
            Instantiate(coin2, transform.position, Quaternion.identity);
            n -= 1;
            time2 = time;

        }
        else if (n < 1 && move)
            Destroy(gameObject);

    }
    private void FixedUpdate()
    {
        if (time <= 1)
            transform.position += new Vector3(0, -s + time / 10, 0);
        time += 0.02f;
    }

    public void take_tax()
    {
        GameObject.Find("Informer").GetComponent<InformerScript>().budget += GetComponentInParent<population_and_nalogs>().tax;
        GetComponentInParent<population_and_nalogs>().tax = 0;
        GetComponentInParent<population_and_nalogs>().status = true;


        move = true;

        this.GetComponent<SpriteRenderer>().enabled = false;
        this.GetComponent<CircleCollider2D>().enabled = false;
    }
}
