﻿using UnityEngine;

public class get_parenr : MonoBehaviour
{
    public int tax;
    void Update()
    {
        if (GameObject.Find("Informer").GetComponent<InformerScript>().active_bottons)
        {
            GameObject.Find("Informer").GetComponent<InformerScript>().active_bottons2 = false;
            Destroy(gameObject);
        }
    }
    public void take_tax()
    {
        if (GetComponentInParent<population_and_nalogs>().tax > 0)
        {
            GameObject.Find("Informer").GetComponent<InformerScript>().budget += GetComponentInParent<population_and_nalogs>().tax;
            GetComponentInParent<population_and_nalogs>().tax = 0;
            GameObject.Find("Informer").GetComponent<InformerScript>().active_bottons2 = false;
            Destroy(gameObject);
        }
    }
}
