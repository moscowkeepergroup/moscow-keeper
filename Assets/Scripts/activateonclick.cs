
using UnityEngine;
using UnityEngine.UI;

public class activateonclick : MonoBehaviour
{
    public InformerScript info;

    public void Update()
    {
        if (info.active_bottons)
        {
            this.GetComponent<Image>().enabled = false;
        }
    }
    public void OnClick()
    {
        this.GetComponent<Image>().enabled = !this.GetComponent<Image>().enabled;
    }
}
