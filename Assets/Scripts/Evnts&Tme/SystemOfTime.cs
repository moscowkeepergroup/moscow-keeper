﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public struct Date
{
    public int years;
    public int months;
    public int days;
    public int hours;
    public string weekday;
    public Date(int nday = 0, int nmonth = 8, int nyear = 2019, int nhours = 0, string nweekday = "Пон")
    {
        years = nyear;
        months = nmonth;
        days = nday;
        hours = nhours;
        weekday = nweekday;
    }
}
public class SystemOfTime : MonoBehaviour
{
    public const double x = 0.3;
    public const int numhouses = ObjectsScript.Size;
    readonly CallEvent StartEvent = new CallEvent();
    public Date NewDate = new Date(0, 8, 2019, 0);
    public static double nowgametime = 0;
    public int count = 0;
    public InformerScript _reputation;
    public ObjectsScript objectscript;
    public int[] DaysInMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    public Date GetingTime()                   // Return GameDate
    {

        string[] DaysOfWeek = { "Пон", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс" };
        if (Time.time - nowgametime > x)
        {
            if ((NewDate.hours == 0) && (HolidaysClass.multiplier == 0))
            {
                this.GetComponent<HolidaysClass>().CheckDay();
            }
            else if ((NewDate.hours == 0) && (HolidaysClass.multiplier > 0))
            {
                _reputation.Reputation /= HolidaysClass.multiplier;
                HolidaysClass.multiplier = 0;

            }
            nowgametime += x;
            NewDate.hours++;

        }
        // skip day
        if (SkipDay.skip == true)
        {
            NewDate.days++;
            NewDate.hours = 0;
            if (count != 6)
            {
                count++;
            }
            else count = 0;
            NewDate.weekday = DaysOfWeek[count];
            SkipDay.skip = false;
        }
        else
        if (NewDate.hours == 24)
        {

            NewDate.days++;
            if (count != 6)
            {
                count++;
            }
            else count = 0;
            NewDate.weekday = DaysOfWeek[count];
            NewDate.hours = 0;
        }
        for (int i = 0; (NewDate.days == DaysInMonth[(int)NewDate.months]); i++)
        {
            NewDate.months++;
            this.GetComponent<DegradationOfBuildings>().MinusMonth();
            this.GetComponent<DegradationOfBuildings>().Update();
            NewDate.days -= DaysInMonth[(int)NewDate.months - 1];
            if (NewDate.months == 12)
            {
                NewDate.years++;
                NewDate.months = 0;
            }
        }
        return NewDate;
    }

    void Start()
    {
        this.GetComponent<DegradationOfBuildings>().Start();
        this.GetComponent<DegradationOfBuildings>().Update();
        this.GetComponent<HolidaysClass>().Start();
        // GameObject.Find("city-map").GetComponent<HolidaysClass>().CheckDay();

        for (int i = 0; i < numhouses; i++)
        {
            try
            {
                objectscript.BUILDINGS[i].objects.GetComponent<Alarm>().Start();
            }
            catch (System.Exception)
            {
                ;
            }
        }
    }
    void Update()
    {
        if (Time.time - nowgametime > x)
        {
            Date nowdate = GetingTime();

            // restore sprites of buildings
            if (ButtonNotif.RestoreTime.Count > 0)
            {
                int z = GetingTime().months - 1 >= 0 ? DaysInMonth[GetingTime().months - 1] : DaysInMonth[11];

                if ((ButtonNotif.RestoreTime.Peek().days + 3 == GetingTime().days) || (z - ButtonNotif.RestoreTime.Peek().days + GetingTime().days == 3))
                {


                    int restoreday = GetingTime().days;
                    while ((ButtonNotif.RestoringBuildings.Count > 0) && (ButtonNotif.RestoreTime.Count > 0) && ((ButtonNotif.RestoreTime.Peek().days + 3 == restoreday) || (z - ButtonNotif.RestoreTime.Peek().days + GetingTime().days == 3)))
                    {
                        ButtonNotif.RestoreTime.Dequeue();


                        objectscript.BUILDINGS[ButtonNotif.RestoringBuildings.Peek()].ServiceLife += 3; // исправить ремонт

                        GameObject House = objectscript.BUILDINGS[ButtonNotif.RestoringBuildings.Dequeue()].objects;

                        for (int j = 0; j < 4; j++)
                        {
                            if (House.GetComponent<SpriteRenderer>().sprite == objectscript.Oldsprites[j])
                            {
                                _reputation.Reputation += InformerScript.coefficients.k3;
                                House.GetComponent<SpriteRenderer>().sprite = objectscript.Newsprites[j];
                                double X = House.transform.position.x;
                                double Y = House.transform.position.y;
                                for (int t = 0; t < objectscript.BUILDINGS.Length; t++)
                                {
                                    GameObject House2 = objectscript.BUILDINGS[t].objects;
                                    if ((Math.Abs((decimal)(House2.transform.position.x - X)) < 3) && (Math.Abs((decimal)(House2.transform.position.y - Y)) < 3))
                                    {

                                        if ((objectscript.BUILDINGS[t].inviscontentment + InformerScript.coefficients.k10) < 100.0)
                                        {
                                            objectscript.BUILDINGS[t].inviscontentment += InformerScript.coefficients.k10;
                                        }
                                        else { objectscript.BUILDINGS[t].inviscontentment = 100; }
                                    }
                                }

                            }
                        }
                        // в конце массивы можно будет слить
                    }
                }
            }

            if (ButtonNotif.RestoreTime.Count > 0)
            {
                int z = GetingTime().months - 1 >= 0 ? DaysInMonth[GetingTime().months - 1] : DaysInMonth[11];

                if ((ButtonNotif.RestoreTime.Peek().days + 3 == GetingTime().days) || (z - ButtonNotif.RestoreTime.Peek().days + GetingTime().days == 3))
                {

                    int restoreday2 = GetingTime().days;
                    while ((ButtonNotif.RestoringRoads.Count > 0) && (ButtonNotif.RestoreTime.Count > 0) && ((ButtonNotif.RestoreTime.Peek().days + 3 == restoreday2) || (z - ButtonNotif.RestoreTime.Peek().days + GetingTime().days == 3)))
                    {
                        ButtonNotif.RestoreTime.Dequeue();

                        objectscript.ROADS[ButtonNotif.RestoringRoads.Peek()].ServiceLife += 3; // исправить ремонт
                        GameObject Road = objectscript.ROADS[ButtonNotif.RestoringRoads.Dequeue()].objects;

                        for (int j = 0; j < 3; j++)
                        {
                            if (Road.GetComponent<SpriteRenderer>().sprite == objectscript.OldspritesRoads[j])
                            {
                                _reputation.Reputation += InformerScript.coefficients.k4;
                                Road.GetComponent<SpriteRenderer>().sprite = objectscript.NewspritesRoads[j];
                            }
                        }
                    }
                }
            }

            StartEvent.Condition(nowdate);
        }
    }

}
public class CallEvent
{
    public int Numhouses = SystemOfTime.numhouses;

    public bool allow;
    public void Condition(Date nowdate)   // the condition for the event to run 
    {

        if ((nowdate.weekday == "Сб") && (allow == true))
        {
            allow = false;
            {
                GameObject.Find("Informer").GetComponent<InformerScript>().code_word = "boont";
                Choise(Numhouses);
            }
        }
        if ((nowdate.weekday != "Сб") && (allow != true) && !GameObject.Find("Informer").GetComponent<InformerScript>().active_bottons)
        {
            allow = true;
        }
    }
    public void Choise(int numhouses)
    {
        int TodayEvent = EventsSystem.GetEvent(numhouses);
        int n = UnityEngine.Random.Range(0, numhouses - 1);
        GameObject.Find("Objecter").GetComponent<ObjectsScript>().BUILDINGS[n].objects.GetComponent<Alarm>().enabled = true;
        GameObject.Find("Objecter").GetComponent<ObjectsScript>().BUILDINGS[n].objects.GetComponent<Alarm>().button = true;
        for (int i = 0; i < numhouses; i++)
        {

            if (TodayEvent == i)
            {
                // testing block
                if (GameObject.Find("Objecter").GetComponent<ObjectsScript>().BUILDINGS[i].ServiceLife > 0)
                {
                    GameObject.Find("Objecter").GetComponent<ObjectsScript>().BUILDINGS[i].ServiceLife--;
                    GameObject.Find("city-map").GetComponent<DegradationOfBuildings>().Update();
                }
                GameObject.Find("Objecter").GetComponent<ObjectsScript>().BUILDINGS[i].objects.GetComponent<Alarm>().Update();
                // testing block
            }
        }
    }
}
public class EventsSystem
{
    public static int SystemOfRandomEvents(double[] Events, int numhouses) // Return random event from the list
    {
    rechoose:
        double sum = 0;
        int a = UnityEngine.Random.Range(1, 100);

        for (int i = 0; i < numhouses; i++)
        {
            if ((a > sum) && (a < Events[i] + sum))
            {
                if (!GameObject.Find("Objecter").GetComponent<ObjectsScript>().BUILDINGS[i].objects.GetComponent<Alarm>())
                {
                    goto rechoose;
                }
                return i;
            }
            sum += Events[i];
        }
        return -1;
    }
    public static int GetEvent(int numhouses)
    {
        double[] Events = new double[numhouses];
        int ChosenEvent;

        for (int i = 0; i < numhouses; i++)
        {
            Events[i] = 100.0 / (double)numhouses;
        }
        ChosenEvent = SystemOfRandomEvents(Events, numhouses);
        return ChosenEvent;
    }
}























