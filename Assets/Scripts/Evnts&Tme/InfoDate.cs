﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class InfoDate : MonoBehaviour
{
    public InformerScript Info;
    public float hours;
    public float days;
    public float months;
    public float years;
    public string weekday;
    void Update()
    {
        hours = Info.time.NewDate.hours;
        days = Info.time.NewDate.days;
        months = Info.time.NewDate.months;
        years = Info.time.NewDate.years;
        weekday = Info.time.NewDate.weekday;
        this.GetComponent<Text>().text = Convert.ToString(Info.time.NewDate.hours) + ":00" + "\n" + "Сегодня " + Convert.ToString((int)(Info.time.NewDate.days + 1)) +
            "." + Convert.ToString((int)(Info.time.NewDate.months + 1)) + "." + Convert.ToString((int)(Info.time.NewDate.years)) + ". " + Info.time.NewDate.weekday;
    }
}
