﻿using UnityEngine;

public class HolidaysClass : MonoBehaviour
{
    public struct Holidays
    {
        public int holiday_day;
        public int holiday_month;
        public string holiday_name;
    };

    // List of holidays (we will save our time in form (t - 1) and output it in correct form)
    public const int numholidays = 18;
    public Holidays[] ListHolidays = new Holidays[numholidays];
    public InformerScript _reputation;
    public static double multiplier = 0;

    public void Start()
    {
        ListHolidays[0].holiday_name = "Новый Год";
        ListHolidays[0].holiday_day = 0;
        ListHolidays[0].holiday_month = 0;

        ListHolidays[1].holiday_name = "День Знаний";
        ListHolidays[1].holiday_day = 0;
        ListHolidays[1].holiday_month = 8;

        ListHolidays[2].holiday_name = "День Победы";
        ListHolidays[2].holiday_day = 8;
        ListHolidays[2].holiday_month = 4;

        ListHolidays[3].holiday_name = "День России";
        ListHolidays[3].holiday_day = 11;
        ListHolidays[3].holiday_month = 5;

        ListHolidays[4].holiday_name = "Старый Новый Год";
        ListHolidays[4].holiday_day = 12;
        ListHolidays[4].holiday_month = 0;

        ListHolidays[5].holiday_name = "День Защитника Отечества";
        ListHolidays[5].holiday_day = 22;
        ListHolidays[5].holiday_month = 1;

        ListHolidays[6].holiday_name = "Международный женский день";
        ListHolidays[6].holiday_day = 7;
        ListHolidays[6].holiday_month = 2;

        ListHolidays[7].holiday_name = "Праздник весны и труда";
        ListHolidays[7].holiday_day = 0;
        ListHolidays[7].holiday_month = 4;

        ListHolidays[8].holiday_name = "Международный день защиты детей";
        ListHolidays[8].holiday_day = 0;
        ListHolidays[8].holiday_month = 5;

        ListHolidays[9].holiday_name = "День Учителя";
        ListHolidays[9].holiday_day = 4;
        ListHolidays[9].holiday_month = 9;

        ListHolidays[10].holiday_name = "День Полиции";
        ListHolidays[10].holiday_day = 9;
        ListHolidays[10].holiday_month = 10;

        ListHolidays[11].holiday_name = "День Cтудентов";
        ListHolidays[11].holiday_day = 24;
        ListHolidays[11].holiday_month = 0;

        ListHolidays[12].holiday_name = "День молодёжи";
        ListHolidays[12].holiday_day = 26;
        ListHolidays[12].holiday_month = 5;

        ListHolidays[13].holiday_name = "Рождество";
        ListHolidays[13].holiday_day = 6;
        ListHolidays[13].holiday_month = 0;

        ListHolidays[14].holiday_name = "День ВДВ";
        ListHolidays[14].holiday_day = 1;
        ListHolidays[14].holiday_month = 7;

        ListHolidays[15].holiday_name = "День космонавтики";
        ListHolidays[15].holiday_day = 11;
        ListHolidays[15].holiday_month = 3;

        ListHolidays[16].holiday_name = "День специалиста по ядерному обеспечению";
        ListHolidays[16].holiday_day = 3;
        ListHolidays[16].holiday_month = 8;

        ListHolidays[17].holiday_name = "День пожарной охраны";
        ListHolidays[17].holiday_day = 29;
        ListHolidays[17].holiday_month = 3;

    }
    public void CheckDay()
    {
        Date holidaydate = GameObject.Find("city-map").GetComponent<SystemOfTime>().NewDate;
        for (int i = 0; i < 18; i++)
        {
            if ((holidaydate.days == ListHolidays[i].holiday_day) && (holidaydate.months == ListHolidays[i].holiday_month))
            {
                Debug.Log(ListHolidays[i].holiday_name);
                if (i < 4)
                {
                    _reputation.Reputation *= InformerScript.coefficients.k5;
                    multiplier = InformerScript.coefficients.k5;
                }
                if ((i >= 4) && (i < 14))
                {
                    _reputation.Reputation *= InformerScript.coefficients.k6;
                    multiplier = InformerScript.coefficients.k6;
                }
                if ((i >= 14) && (i < 18))
                {
                    _reputation.Reputation *= InformerScript.coefficients.k7;
                    multiplier = InformerScript.coefficients.k7;
                }
            }
        }
    }
}
