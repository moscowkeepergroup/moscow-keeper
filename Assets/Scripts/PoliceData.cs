﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PoliceData : MonoBehaviour
{
    public TextAsset data;
    public string information;
    public string[] letters;
    public Text Name;
    public Text Info;
    public Text Chara;
    public GameObject PicMent;
    public GameObject PicBob;
    public GameObject PicParent;
    public  Text cost;
    public GameObject pic;
    int pos;
    public InformerScript inform;
    public Text count;
    string x;
    string y;
    public GameObject malo;
    string k;
    public GameObject policemen;
    public GameObject bobicman;
    public GameObject PoliceOffice;
    private void Start()
    {
        PoliceOffice = GameObject.Find("полиция");
        inform = GameObject.Find("Informer").GetComponent<InformerScript>();
        inform.active_bottons = true;
        inform.active_bottons2 = true;
        inform.pause = true;
        data = (TextAsset)Resources.Load("police");
        information = data.ToString();
        loader();
        policers();
    }
    private void Update()
    {
        count.text = "Вызвать "+x + '\n' + "Доступно: "+ y ;
       
    }
    void loader()
    {
       
        
        letters = information.Split('\n');


    }

    public void policers()
    {
        Destroy(pic);
        pos = letters[0].IndexOf('|');
        Name.text = letters[0].Substring(0, pos - 1);
        letters[0] = letters[0].Substring(pos + 1);
        pos = letters[0].IndexOf('|');
        Info.text = letters[0].Substring(0, pos - 1);
        letters[0] = letters[0].Substring(pos + 1);
        pos = letters[0].IndexOf('|');
        Chara.text = letters[0].Substring(0, pos - 1);
        letters[0] = letters[0].Substring(pos + 1);
        cost.text = "Нанять" + '\n' + letters[0];
        pic = Instantiate(PicMent, PicParent.transform.position, Quaternion.identity, PicParent.transform);
        x = "отряд(х5)";
        y = inform.police_count.ToString();
        k = "police";
        loader();


    }
    public void bobiks()
    {
        Destroy(pic);
        pos = letters[1].IndexOf('|');
        Name.text = letters[1].Substring(0, pos - 1);
        letters[1] = letters[1].Substring(pos + 1);
        pos = letters[1].IndexOf('|');
        Info.text = letters[1].Substring(0, pos - 1);
        letters[1] = letters[1].Substring(pos + 1);
        pos = letters[1].IndexOf('|');
        Chara.text = letters[1].Substring(0, pos - 1);
        letters[1] = letters[1].Substring(pos + 1);
        cost.text = "Нанять"+ '\n'+letters[1];
        pic = Instantiate(PicBob, PicParent.transform.position, Quaternion.identity, PicParent.transform);
        y = inform.bobic_count.ToString();
        k = "bobic";
        x = "";
        loader();
    }
    public void close()
    {
        inform.active_bottons =false;
        inform.active_bottons2 = false;
        inform.pause = false;
        Destroy(gameObject);
    }
    public void add()
    {
        switch (k)
        {
            case "police": addpolice();
                break;
            case "bobic": addbobik();
                break;

        }
    }
    public void addpolice()
    {
        if (inform.budget >= 10000)
        {
            inform.budget -= 10000;
            inform.police_count++;
            y = inform.police_count.ToString();
            count.text = "Вызвать " + x + '\n' + "Доступно: " + y;
        }
        else
        {
            Instantiate(malo, transform.position, Quaternion.identity);
            close();
        }
    }
    public void addbobik()
    {
        if (inform.budget >= 15000)
        {
            inform.budget -= 50000;
            inform.bobic_count++;
            y = inform.bobic_count.ToString();
            count.text = "Вызвать " + x + '\n' + "Доступно: " + y;

        }
        else
        {
            Instantiate(malo, transform.position, Quaternion.identity);
            close();
        }
    }
    public void call()
    {
        switch (k)
        {
            case "police":
                callpolice();
                break;
            case "bobic":
                callbobic();
                break;

        }
    }
    public void callpolice()
    {
        if (inform.police_count >= 5)
        {
            Instantiate(policemen, PoliceOffice.transform.position + new Vector3(0.5f, -2, 0), Quaternion.identity);
            inform.police_count -= 5;
            close();
        }
    }
    public void callbobic()
    {
        if (inform.bobic_count >0)
        {
            Instantiate(bobicman, PoliceOffice.transform.position + new Vector3(2, -2.3f, 0), Quaternion.identity);
            inform.bobic_count --;
            close();
        }
    }
}
