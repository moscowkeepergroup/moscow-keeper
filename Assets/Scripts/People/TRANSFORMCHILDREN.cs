﻿using System.Collections.Generic;
using UnityEngine;
public class TRANSFORMCHILDREN : MonoBehaviour
{
    List<Vector2Int> way;
    public int ind;
    public int prevind;
    Vector3 delta;
    public InformerScript Info;
    public float speed;
    public float lastWaypointSwitchTime;
    public void Start()
    {
        speed = 1;
        way = GameObject.Find("Snowman").GetComponent<Djiikstra>().wp;
    }
    public Vector2 Index_to_Pos(Vector2Int ind)
    {
        Vector2 res = new Vector2Int();
        res.x = ind.x * Info.cam_Width * 1.1f / Info.discrete + Info.cam_Left;//(int)(((Q.x - Info.cam_Left) * Info.discrete / 1.1f) / Info.cam_Width);
        res.y = -1 * ((ind.y * Info.cam_Height / Info.discrete) - Info.cam_Top);//(int)(((Info.cam_Top - Q.y) * Info.discrete) / Info.cam_Height);
        return res;
    }
    void Update()
    {
        ind = GameObject.Find("Snowman").GetComponent<Djiikstra>().cur_wp;
        if (way.Count > 0)
        {
            if (prevind != ind)
            {
                prevind = ind;
                if (this.transform.position.x > Index_to_Pos(way[ind]).x)
                {
                    delta.x = this.transform.position.x - 1;
                }
                else if (this.transform.position.x == Index_to_Pos(way[ind]).x) { delta.x = this.transform.position.x; }
                else if (this.transform.position.x < Index_to_Pos(way[ind]).x) { delta.x = this.transform.position.x + 1; }

                if (this.transform.position.y > Index_to_Pos(way[ind]).y)
                {
                    delta.y = this.transform.position.y - 1;
                }
                else if (this.transform.position.y == Index_to_Pos(way[ind]).y) { delta.y = this.transform.position.y; }
                else if (this.transform.position.y < Index_to_Pos(way[ind]).y) { delta.y = this.transform.position.y + 1; }
                lastWaypointSwitchTime = Time.time;
            }
            Vector2 startPosition = this.transform.position;
            Vector2 endPosition = delta;
            float pathLength = Vector2.Distance(startPosition, endPosition);
            float totalTimeForPath = pathLength / speed;
            float currentTimeOnPath = Time.time - lastWaypointSwitchTime;
            if ((currentTimeOnPath > 0) && (totalTimeForPath > 0))
            {
                gameObject.transform.position = Vector2.Lerp(startPosition, endPosition, currentTimeOnPath / totalTimeForPath);
            }
        }
    }
}
