﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
public class Move_To2 : MonoBehaviour
{
    public InformerScript Info;
    public ObjectsScript Objects;
    public Characteristics character;
    public GameObject[] Purpose;
    public GroupCharacteristic groupchar;
    bool flag, flag2;
    float time;
    float time_edge;
    float speed;
    public GameObject runpoint;
    public void Run()
    {
        Purpose = GameObject.FindGameObjectsWithTag("PlayerSquads");
        for (int i = 0; i < Purpose.Length; i++)
        {
            if (Mathf.Sqrt(Mathf.Pow((float)Purpose[i].transform.position.x - this.transform.position.x, 2) + Mathf.Pow((float)Purpose[i].transform.position.y - this.transform.position.y, 2)) < 5)
            {
                if ((Purpose[i].transform.childCount - 2 >= this.transform.childCount) && (this.groupchar.morale < 60))
                {

                    runpoint.transform.position = this.transform.position + (this.transform.position - Purpose[i].transform.position) * 2;
                    GetComponent<AIDestinationSetter>().target = runpoint.transform;
                }
            }
        }
    }
    public void PurposeForAttack()
    {
        Purpose = GameObject.FindGameObjectsWithTag("PlayerSquads");
        for (int i = 0; i < Purpose.Length; i++)
        {
            if (Mathf.Sqrt(Mathf.Pow((float)Purpose[i].transform.position.x, 2) + Mathf.Pow((float)Purpose[i].transform.position.y, 2)) < 20)
            {
                if (Purpose[i].transform.childCount < this.transform.childCount)
                {

                    GetComponent<AIDestinationSetter>().target = Purpose[i].transform.GetChild(0).transform;
                    groupchar.atack = true;
                }
            }
        }

    }
    private void Start()
    {
        flag = false;
        time_edge = Random.Range(4, 6);
        Info = GameObject.Find("Informer").GetComponent<InformerScript>();
        Objects = GameObject.Find("Objecter").GetComponent<ObjectsScript>();
        //runpoint = this.transform.parent.Find("runpoint").gameObject;
        if (groupchar == null)
        {
            groupchar = this.transform.parent.gameObject.GetComponent<GroupCharacteristic>();
        }
    }
    public bool wait()
    {
        time = time + Time.deltaTime;
        if (time > time_edge)
        {
            time = 0;
            return true;
        }
        else return false;
    }
    private void Update()
    {
        // 3
        if (this.CompareTag("CrowdSquads") && (GetComponent<AIDestinationSetter>().target != Objects.Capitol.transform))
        {
            if ((groupchar.angry > 200) && (groupchar.morale > 80))
            {
                groupchar.atack = false;
                Debug.Log("Capitol");
                GetComponent<AIDestinationSetter>().target = Objects.Capitol.transform;
            }
        }
        // 4
        if (this.CompareTag("CrowdSquads"))
        {
            Run();
        }
        
        
        // 1
        if (this.CompareTag("CrowdSquads") && (GetComponent<AIDestinationSetter>().target == null))
        {
            groupchar.atack = false;
            if ((groupchar.angry < 70) || (Purpose.Length == 0))
            {
                Debug.Log("Boont point");
                Info.choosenPoint = Objects.points[Random.Range(0, Objects.points.Length - 1)];
                GetComponent<AIDestinationSetter>().target = Info.choosenPoint.transform;

            }
        }

        

        // 2
        if (this.CompareTag("CrowdSquads"))
        {
            if (((groupchar.angry < 200) && (groupchar.angry > 70)) || ((groupchar.angry > 200) && (groupchar.morale < 80)))
            {

                StartCoroutine(waiter());
                IEnumerator waiter()
                {
                    yield return new WaitForSecondsRealtime(2);
                    PurposeForAttack();
                }
            }
        }
        




        // in null
        if (this.CompareTag("CrowdSquads") && (GetComponent<AIDestinationSetter>().target != null))
        {

            if (Mathf.Sqrt(Mathf.Pow(((float)this.transform.position.x - (float)Info.choosenPoint.transform.position.x), 2) + Mathf.Pow(((float)this.transform.position.y - (float)Info.choosenPoint.transform.position.y), 2)) < 0.5)
            {
                if (wait())
                {
                    for (int j = 0; j < this.transform.childCount; j++)
                    {
                        if (this.transform.GetChild(j).CompareTag("Human"))
                        { this.transform.GetChild(j).GetComponent<AIDestinationSetter>().target = null; }
                    }
                    GetComponent<AIDestinationSetter>().target = null;
                }

            }
        }

        if (!this.CompareTag("CrowdSquads") && (GetComponent<AIDestinationSetter>().target != this.transform.parent.GetComponent<AIDestinationSetter>().target) && (this.transform.parent.GetComponent<AIDestinationSetter>().target != null))
        {
            GetComponent<AIDestinationSetter>().target = this.transform.parent.GetComponent<AIDestinationSetter>().target;
        }



    }
}
