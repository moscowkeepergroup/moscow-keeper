﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupCharacteristic : MonoBehaviour
{
    public ObjectsScript Objects;
    int prevcount;
    public double morale;
    /*{
        set
        {
            morale = value;
            if (value > 100)
            {
                value = 100.0;
            }
            if (value < 0)
            {
                value = 0.0;
            }
        }
        get
        {
            return morale;
        }
    }
    */
    public double angry;
    /* {
         set
         {
             angry = value;
             if (value > 200)
             {
                 value = 200.0;
             }
             if (value < 0)
             {
                 value = 0.0;
             }
         }
         get
         {
             return angry;
         }
     }*/

    public bool atack;
    float time = 0;
    // Start is called before the first frame update
    void Start()
    {
        Objects = GameObject.Find("Objecter").GetComponent<ObjectsScript>();
        double reputation = GameObject.Find("Informer").GetComponent<InformerScript>().Reputation;
        if (this.gameObject.CompareTag("CrowdSquads"))
        {
            if (reputation < 0)
            {
                Debug.Log("angry");
                angry = (reputation * (-1)) * 1.5;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.CompareTag("CrowdSquads"))
        {
            time = time + Time.deltaTime; // delay in seconds
            if (time > 2)
            {
                time = 0;
                angry++;
                if (prevcount != this.gameObject.transform.childCount)
                {
                    morale = ((float)this.gameObject.transform.childCount / 20f) * 100;
                    prevcount = this.gameObject.transform.childCount;
                }

                float difference = (this.gameObject.transform.position - Objects.Capitol.transform.position).magnitude;
                if (difference < 2.5)
                {
                    Debug.Log("near");
                    morale += 8;
                }
            }

        }
    }
}

