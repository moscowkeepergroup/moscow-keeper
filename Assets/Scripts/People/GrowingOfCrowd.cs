﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowingOfCrowd : MonoBehaviour
{
    Collider CallingZone;
    public Vector3 PointOfAppear;
    public ObjectsScript Objects;
    float time;
    float time_edge;
    private void Start()
    {
        time_edge = Random.Range(10, 15);
        Objects = GameObject.Find("Objecter").GetComponent<ObjectsScript>();

    }


    void OnTriggerStay2D(Collider2D TouchingZone) //
    {

        if (TouchingZone.gameObject.CompareTag("HomeSprite"))
        { 
            time = time + Time.deltaTime;
            if (time > 4)
            {
                time = 0;
                PointOfAppear = TouchingZone.gameObject.transform.GetChild(0).transform.position;
                if (this.transform.childCount < 20)
                {
                    this.transform.GetComponent<GroupCharacteristic>().morale += 1;
                    Instantiate(Objects.human, PointOfAppear, this.transform.rotation, this.transform);
                }
            }
        }

    }

    void Update()
    {

    }
}
