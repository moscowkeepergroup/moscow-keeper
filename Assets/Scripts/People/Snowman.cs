﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snowman : MonoBehaviour
{
    public Vector2 v;
    public Animator animator;
    private void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow)) animator.SetBool("IsRunningRight", true);
        else
        {
            animator.SetBool("IsRunningRight", false);
            if (Input.GetKey(KeyCode.LeftArrow)) animator.SetBool("IsRunningLeft", true);
            else
            {
                animator.SetBool("IsRunningLeft", false);
                if (Input.GetKey(KeyCode.UpArrow)) animator.SetBool("IsRunningUp", true);
                else
                {
                    animator.SetBool("IsRunningUp", false);
                    if (Input.GetKey(KeyCode.DownArrow)) animator.SetBool("IsRunningDown", true);
                    else animator.SetBool("IsRunningDown", false);
                }
            }
        }
        v.x = 0.1f * Input.GetAxis("Horizontal");
        v.y = 0.1f * Input.GetAxis("Vertical");
        this.transform.Translate(v);
    }
}
