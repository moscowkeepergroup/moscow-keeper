﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Characteristics : MonoBehaviour
{
    public int health;
    bool aa = true;
    public int damage;
    GroupCharacteristic groupchar;
    public InformerScript Info;
    public int days;
    public int boont;
    void Start()
    {
        Info = GameObject.Find("Informer").GetComponent<InformerScript>();
        groupchar = this.transform.parent.gameObject.GetComponent<GroupCharacteristic>();
    }
    public void CheckGroup()
    {
        if (this.transform.parent.CompareTag("PlayerSquads"))
        {
            if (this.transform.parent.childCount == 3)
            {
                Destroy(this.transform.parent.gameObject);
            }
        }
        if (this.transform.parent.CompareTag("CrowdSquads"))
        {
            if (this.transform.parent.childCount == 2)
            {
                Destroy(this.transform.parent.gameObject);
                Info.crowd_count--;
            }
        }
    }
    void Update()
    {
        if (health <= 0)
        {
            if (this.CompareTag("Human"))
            {
                this.transform.parent.GetComponent<GroupCharacteristic>().angry += 40;
                this.transform.parent.GetComponent<GroupCharacteristic>().morale -= 3;
            }
            CheckGroup();
            Destroy(gameObject);
        }
       
    }
    private void OnCollisionEnter2D(Collision2D fight)
    {
        if (aa && fight.gameObject.tag == "bobik")
        {
            aa = false;
            Debug.Log("ff");

            health -= Random.Range(15, 25);
        }
        if (((fight.gameObject.tag == "Human") && (this.tag != "Human")) || ((fight.gameObject.tag == "Player") && (this.tag != "Player")))
        {


            StartCoroutine(waiter());
            IEnumerator waiter()
            {
                yield return new WaitForSecondsRealtime(2);
                if ((fight != null) || (fight.gameObject != null))
                {
                    try
                    {
                        fight.gameObject.GetComponent<Characteristics>().health -= damage;
                    }
                    catch (System.Exception)
                    {
                        ;
                    }
                    try
                    {
                        if (this.GetComponent<CircleCollider2D>().IsTouching(fight.collider))
                        {
                            OnCollisionEnter2D(fight);
                        }
                    }
                    catch (System.Exception)
                    {
                        ;
                    }
                }
            }
        }
    }     
}



