﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class ControlGroup : MonoBehaviour
{
    public InformerScript Info;
    public GameObject Point;
    public Camera cam;
    private Collider2D collider;
    public bool flag1 = false;

    public Collider2D Collider { get => collider; set => collider = value; }

    // Start is called before the first frame update
    void Start()
    {
        Info = GameObject.Find("Informer").GetComponent<InformerScript>();
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();

    }

    void Update()
    {

        if (Info.isMouseButtonLeft == true)
        {

            flag1 = false;
            if (this.CompareTag("PlayerSquads"))
            {
                this.transform.parent.gameObject.GetComponent<GroupCharacteristic>().atack = false;
            }
            Point.transform.position = cam.ScreenToWorldPoint(Input.mousePosition);
            Point.GetComponent<CircleCollider2D>().enabled = true;
            this.GetComponent<ControlGroup>().enabled = false;
        }
    }

}
// почему они сбегаются в одну точку
// как проверять на вхождение в другие коллайдеры
