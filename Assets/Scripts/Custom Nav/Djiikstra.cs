﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Elem : IComparable
{
    public float distance;
    public Vector2Int coord;
    public int CompareTo(object o)
    {
        Elem obj = o as Elem;
        if (this.distance > obj.distance) return 1;
        else if (this.distance == obj.distance)
        {
            if (this.coord.y > obj.coord.y) return 1;
            else if (this.coord.y == obj.coord.y)
            {
                if (this.coord.x > obj.coord.x) return 1;
                else if (this.coord.x == obj.coord.x) return 0;
                else return -1;
            }
            else return -1;
        }
        else return -1;
    }
    public override bool Equals(System.Object obj)
    {
        var a = (Elem)obj;
        return a.coord == coord && a.distance == distance;
    }
    public override int GetHashCode()
    {
        return ((int)(coord.x) << 2) ^ (int)(coord.y);
    }
    public static bool operator ==(Elem a, Elem b)
    {
        return a.coord == b.coord && a.distance == b.distance;
    }
    public static bool operator !=(Elem a, Elem b)
    {
        return !(a.coord == b.coord && a.distance == b.distance);
    }
    public static bool operator >(Elem a, Elem b)
    {
        return a != b && !(a.distance < b.distance || a.distance == b.distance && a.coord.y < b.coord.y || a.distance == b.distance && a.coord.y == b.coord.y && a.coord.x < b.coord.x);
    }
    public static bool operator <(Elem a, Elem b)
    {
        return a.distance < b.distance || a.distance == b.distance && a.coord.y < b.coord.y || a.distance == b.distance && a.coord.y == b.coord.y && a.coord.x < b.coord.x;
    }
}
public class Djiikstra : MonoBehaviour
{
    public ObjectsScript Objects;
    public InformerScript Info;
    public Gridd grid;
    public float[,] dist;
    public Vector2Int[,] parent;
    public Elem begin;
    public SortedSet<Elem> s;
    const long INF = 1000000000;
    public List<Vector2Int> wp;
    public int cur_wp;
    public float lastWaypointSwitchTime;
    public float speed;
    Vector2Int Mouse_posAsIndex()
    {
        var res = new Vector2Int
        {
            x = (int)(((Info.mouse_Pos.x - Info.cam_Left) * Info.discrete / 1.1f) / Info.cam_Width),
            y = (int)(((Info.cam_Top - Info.mouse_Pos.y) * Info.discrete) / Info.cam_Height)
        };
        return res;
    }
    Vector2Int Pos_to_Index(Vector3 Q)
    {
        var res = new Vector2Int
        {
            x = (int)(((Q.x - Info.cam_Left) * Info.discrete / 1.1f) / Info.cam_Width),
            y = (int)(((Info.cam_Top - Q.y) * Info.discrete) / Info.cam_Height)
        };
        return res;
    }
    Vector2 Index_to_Pos(Vector2Int ind)
    {
        Vector2 res = new Vector2
        {
            x = ind.x * Info.cam_Width * 1.1f / Info.discrete + Info.cam_Left,
            y = -1 * ((ind.y * Info.cam_Height / Info.discrete) - Info.cam_Top)
        };
        return res;
    }
    void Move(int y, int x, int dx, int dy)
    {
        if (y + dy >= 0 && y + dy <= Info.discrete && x + dx >= 0 && x + dx <= Info.discrete / 1.1f)
        {
            var delta = ((dx == -1 && dy == 1 || dx == 1 && dy == -1 || dx == 1 && dy == 1 || dx == -1 && dy == -1) ? 1.4866f : 1.0f);
            if ((dist[y, x] + delta < dist[y + dy, x + dx]) && grid.used[y + dy, x + dx] == true)
            {
                Elem pos = new Elem { coord = new Vector2Int { x = x + dx, y = y + dy }, distance = dist[y + dy, x + dx] };
                dist[y + dy, x + dx] = dist[y, x] + delta;
                s.Remove(pos);
                pos.distance = dist[y + dy, x + dx];
                s.Add(pos);
                parent[y + dy, x + dx].y = y;
                parent[y + dy, x + dx].x = x;
            }
        }
    }
    void Dijkstra()
    {
        while (s.Count > 0)
        {
            Elem vec = s.Min;
            s.Remove(s.Min);
            for (int dir_x = -1; dir_x <= 1; dir_x++)
                for (int dir_y = -1; dir_y <= 1; dir_y++)
                    Move(vec.coord.y, vec.coord.x, dir_x, dir_y);
        }
    }
    void Start()
    {
        speed = 1.9f;
        s = new SortedSet<Elem>();
        dist = new float[Info.discrete + 1, (int)(Info.discrete / 1.1f) + 1];
        for (int ii = 0; ii <= Info.discrete; ii++)
            for (int jj = 0; jj <= (int)(Info.discrete / 1.1f); jj++)
                dist[ii, jj] = INF;
        parent = new Vector2Int[Info.discrete + 1, (int)(Info.discrete / 1.1f) + 1];
        begin = new Elem();
        cur_wp = 0;
    }
    void Update()
    {
        if (Info.isMouseButtonLeft && grid.used[Mouse_posAsIndex().y, Mouse_posAsIndex().x] == true)
        {
            cur_wp = 0;
            var x = Pos_to_Index(this.transform.position).x;
            var y = Pos_to_Index(this.transform.position).y;
            begin.coord.x = x;
            begin.coord.y = y;
            begin.distance = 0;
            for (int i = 0; i < Info.discrete + 1; i++)
                for (int j = 0; j < (int)(Info.discrete / 1.1f) + 1; j++)
                    dist[i, j] = INF;
            wp.Clear();
            dist[begin.coord.y, begin.coord.x] = 0;
            parent[begin.coord.y, begin.coord.x].y = parent[begin.coord.y, begin.coord.x].x = -1;
            s.Add(begin);
            Dijkstra();
            int endX = Mouse_posAsIndex().x;
            int endY = Mouse_posAsIndex().y;
            while (endX != begin.coord.x || endY != begin.coord.y)
            {
                wp.Add(new Vector2Int(endX, endY));
                int tmp = endX;
                endX = parent[endY, endX].x;
                endY = parent[endY, tmp].y;
            }
            wp.Add(new Vector2Int(begin.coord.x, begin.coord.y));
            wp.Reverse();
        }
        if (cur_wp <= wp.Count - 2)
        {
            var startPosition = Index_to_Pos(wp[cur_wp]);
            var endPosition = Index_to_Pos(wp[cur_wp + 1]);
            var pathLength = Vector2.Distance(startPosition, endPosition);
            var totalTimeForPath = pathLength / speed;
            var currentTimeOnPath = Time.time - lastWaypointSwitchTime;
            gameObject.transform.position = Vector2.Lerp(startPosition, endPosition, currentTimeOnPath / totalTimeForPath);
            if (gameObject.transform.position.x == endPosition.x && gameObject.transform.position.y == endPosition.y)
                if (cur_wp < wp.Count - 2)
                {
                    cur_wp++;
                    lastWaypointSwitchTime = Time.time;
                }
        }
    }
}
