﻿using UnityEngine;
public class Gridd : MonoBehaviour
{
    public ObjectsScript Objects;
    public InformerScript Info;
    public Vector2 coord_pos;
    public GameObject[,] Nodes;
    public bool[,] used;

    public bool flag;
    void Start()
    {
        used = new bool[Info.discrete + 1, (int)(Info.discrete / 1.1f) + 1];
        flag = false;
        Nodes = new GameObject[Info.discrete + 1, (int)(Info.discrete / 1.1f) + 1];
        int i = 0, j = 0;
        coord_pos = new Vector2(Info.cam_Left, Info.cam_Top);
        for (coord_pos.y = Info.cam_Top; coord_pos.y >= Info.cam_Bottom; coord_pos.y -= Info.cam_Height / Info.discrete)
        {
            for (coord_pos.x = Info.cam_Left; coord_pos.x <= Info.cam_Right; coord_pos.x += Info.cam_Width / Info.discrete * 1.1f)
            {
                Nodes[i, j] = Instantiate(Objects.Node, coord_pos, this.transform.rotation, this.transform);
                j++;
            }
            i++;
            j = 0;
        }
    }
    void Update()
    {
        if (Time.time > Time.deltaTime && !flag)
        {
            flag = true;
            for (int i = 0; i < Info.discrete + 1; i++)
                for (int j = 0; j < (int)(Info.discrete / 1.1f) + 1; j++)
                    if (Nodes[i, j] == null)
                        used[i, j] = false;
                    else
                    {
                        Destroy(Nodes[i, j]);
                        used[i, j] = true;
                    }
        }
    }
}
