﻿using UnityEngine;

public class Node : MonoBehaviour
{
    public void OnCollisionEnter2D(Collision2D s)
    {
        if (s.transform.tag != "Human")
        {
            Destroy(gameObject);
        }
    }
}
