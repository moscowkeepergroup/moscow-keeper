﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Makeexplosion : MonoBehaviour
{
    public InformerScript Info;
    public Camera cam;
    public GameObject explosion_point;
    public Collider2D[] victims;
    public float explosionRadius;// радиус поражения
    public float power;// сила взрыва	
    public float deadliness;
    public float delay_time;
    private float time;
    public float hits;
    public float balance_hits;
    public bool AllowForBombing;
    // Start is called before the first frame update
    void Start()
    {
        Info = GameObject.Find("Informer").GetComponent<InformerScript>();
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        balance_hits = hits;
    }
    // Update is called once per frame

    void Update()
    {

        if (Info.isMouseButtonLeft == true)
        {
            Debug.Log("ok");
            if (explosion_point!=null) explosion_point.transform.position = cam.ScreenToWorldPoint(Input.mousePosition);
            time = delay_time;
            AllowForBombing = true;
        }
        if (AllowForBombing == true)
        {
            time = time + Time.deltaTime;
            if (balance_hits > 0)
            {

                if (time > delay_time)
                {
                    time = 0;

                    victims = Physics2D.OverlapCircleAll((Vector2)explosion_point.transform.position, 2);
                    for (int i = 0; i < victims.Length; i++)
                    {
                        if (victims[i].transform.gameObject.GetComponent<Rigidbody2D>() != null)
                        {
                            float length = ((Vector2)victims[i].transform.gameObject.transform.position - (Vector2)explosion_point.transform.position).magnitude;

                            if (victims[i].transform.gameObject.GetComponent<Rigidbody2D>() != null) { victims[i].transform.gameObject.GetComponent<Rigidbody2D>().AddForce(((Vector2)victims[i].transform.gameObject.transform.position - (Vector2)explosion_point.transform.position) * power); }
                            if (victims[i].transform.gameObject.GetComponent<Characteristics>() != null)
                            {
                                victims[i].transform.gameObject.GetComponent<Characteristics>().health = victims[i].transform.gameObject.GetComponent<Characteristics>().health - (int)(deadliness * length / explosionRadius);
                            }
                            Debug.Log((int)(deadliness * explosionRadius / length));
                        }
                    }
                    balance_hits--;



                }
            }
            else
            {
                balance_hits = hits;
                victims = null;
                this.enabled = false;
                AllowForBombing = false;
            }
        }
    }
}