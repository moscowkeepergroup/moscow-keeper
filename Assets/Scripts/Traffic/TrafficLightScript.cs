﻿using System.Collections;
using UnityEngine;

public class TrafficLightScript : MonoBehaviour
{
    public byte status;
    void Start()
    {
        this.status = 1;
        red.SetActive(false);
        yellow.SetActive(false);
        green.SetActive(false);
        switch (status)
        {
            case 1:
                red.SetActive(true);
                break;
            case 2:
                green.SetActive(true);
                break;
        }
        StartCoroutine(UpdateLight());
    }
    IEnumerator UpdateLight()
    {
        while (true)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(1, 3));
            switch (status)
            {
                case 1:
                    status = 2;
                    break;
                case 2:
                    status = 1;
                    break;
            }
            red.SetActive(false);
            green.SetActive(false);
            yield return new WaitForSeconds(1);
            yellow.SetActive(true);
            yield return new WaitForSeconds(UnityEngine.Random.Range(1f, 3f));
            yellow.SetActive(false);
            yield return new WaitForSeconds(1);
            switch (status)
            {
                case 1:
                    red.SetActive(true);
                    break;
                case 2:
                    green.SetActive(true);
                    break;
            }
        }
    }
    public GameObject red;
    public GameObject yellow;
    public GameObject green;
}
