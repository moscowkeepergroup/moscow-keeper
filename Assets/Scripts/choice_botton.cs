﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class choice_botton : MonoBehaviour
{

    public string p1;
    public string p2;
    public void pos1()
    {
        switch (p1)
        {
            case "Р+":
                GameObject.Find("Informer").GetComponent<InformerScript>().Reputation += 5;
                break;
            case "Р-":
                GameObject.Find("Informer").GetComponent<InformerScript>().Reputation -= 5;
                break;
            case "Р?":
                GameObject.Find("Informer").GetComponent<InformerScript>().Reputation += Random.Range(-5, 5);
                break;
            case "Б-":
                GameObject.Find("Informer").GetComponent<InformerScript>().budget -= Random.Range(100000, 500000);
                break;
        }

        FindObjectOfType<AudioManager>().Play("Beep");

        GameObject.Find("Informer").GetComponent<InformerScript>().active_bottons = false;
        GameObject.Find("Informer").GetComponent<InformerScript>().pause = false;
        Destroy(gameObject);
    }
    public void neg2()
    {
        switch (p2)
        {
            case "Р+":
                GameObject.Find("Informer").GetComponent<InformerScript>().Reputation += 5;
                break;
            case "Р-":
                GameObject.Find("Informer").GetComponent<InformerScript>().Reputation -= 5;
                break;
            case "Р?":
                GameObject.Find("Informer").GetComponent<InformerScript>().Reputation += Random.Range(-5, 5);
                break;
            case "Б-":
                GameObject.Find("Informer").GetComponent<InformerScript>().budget -= Random.Range(100000, 500000);
                break;
        }

        FindObjectOfType<AudioManager>().Play("Beep");

        GameObject.Find("Informer").GetComponent<InformerScript>().active_bottons = false;
        GameObject.Find("Informer").GetComponent<InformerScript>().pause = false;
        Destroy(gameObject);
    }
}
