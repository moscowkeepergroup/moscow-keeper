﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MafiaInHouse : MonoBehaviour
{
    private int _capturedFloors;

    private population_and_nalogs _populationAndNalogs;

    [SerializeField] private float _timeForCapture;

    [SerializeField] private int _maxFloor;

    private TextMesh _textMesh;

    // Start is called before the first frame update
    void Start()
    {
        _capturedFloors = 1;
        _populationAndNalogs = GetComponentInParent<population_and_nalogs>();
        StartCoroutine(CaptureFloor(_timeForCapture));
        _textMesh = GetComponent<TextMesh>();
        _textMesh.text = _capturedFloors.ToString();
    }

    private IEnumerator CaptureFloor(float time)
    {
        if (_capturedFloors < _maxFloor)
        {
            yield return new WaitForSeconds(time);
            CapturedFloorsIncrease(1);
            StartCoroutine(CaptureFloor(time));
        }
    }

    private void CapturedFloorsIncrease(int amount)
    {
        _capturedFloors += amount;
        _textMesh.text = _capturedFloors.ToString();
    }
    
}
