﻿using System.Collections;
using System.Collections.Generic;
using Pathfinding.Util;
using UnityEngine;

public class MafiaManager : MonoBehaviour
{
    [SerializeField] private GameObject prefab;
    
    [SerializeField] private GameObject housesGroup;

    [SerializeField] private float deviate;
    
    [SerializeField] private float _timeForRetry;
    
    private List<Transform> _housesList;

    [SerializeField] private float probability;

    [SerializeField] private GameObject informer;

    [SerializeField] private float _waitForRepeat;

    private bool _isActive;
    
    private InformerScript _informerScript;
    
    // Start is called before the first frame update
    void Start()
    {
        _informerScript = informer.GetComponent<InformerScript>();
        _housesList = new List<Transform>();
        _isActive = false;
    }

    void Update()
        // Update is called once per frame
    {
        if (_isActive) return;
        
        double proba = (_informerScript.Reputation + 100) / 200;
        float randomValue = Gaussian.NextGaussian(0, deviate);
        _housesList.ClearFast();
        _isActive = true;
        StartCoroutine(ChangeActive());
        if (randomValue < 0 || randomValue > 1) return;
        
        if (randomValue < proba) return;
        Transform houseGroup = housesGroup.transform;
        for (int i = 0; i < houseGroup.childCount; i++)
        {
            Transform child = houseGroup.GetChild(i);
            if (CheckObsoleteBuild(child))
                _housesList.Add(child);
        }
        if (_housesList.Count == 0) return;
        int houseNum = Random.Range(0, _housesList.Count - 1);
        Transform house = _housesList[houseNum];
        if (!MafiaExists(house))
            Instantiate(prefab, house.transform.position, Quaternion.identity, house);
        _housesList.Clear();
        _isActive = true;
        StartCoroutine(SetActiveFalse());
    }

    private IEnumerator ChangeActive()
    {
        yield return new WaitForSeconds(_timeForRetry);
        _isActive = false;
    }
    
//    void Update()
//    {
//        if (_isActive) return;
//        
//        _housesList.ClearFast();
//        if (Random.Range(0.0f, 100.0f) > probability)
//            return;
////        foreach (Transform child in housesGroup.transform.GetComponentsInChildren<Transform>())
//        Transform houseGroup = housesGroup.transform;
//        for (int i = 0; i < houseGroup.childCount; i++)
//        {
//            Transform child = houseGroup.GetChild(i);
//            if (CheckObsoleteBuild(child))
//                _housesList.Add(child);
//        }
//        if (_housesList.Count == 0) return;
//        int houseNum = Random.Range(0, _housesList.Count - 1);
//        Transform house = _housesList[houseNum];
//        if (!MafiaExists(house))
//            Instantiate(prefab, house.transform.position, Quaternion.identity, house);
//        _housesList.Clear();
//        _isActive = true;
//        StartCoroutine(SetActiveFalse());
//    }

    private IEnumerator SetActiveFalse()
    {
        yield return new WaitForSeconds(_waitForRepeat);
        _isActive = false;
    }
    
    private bool MafiaExists(Transform house)
    {
        for (int i = 0; i < house.childCount; i++)
        {
            if ("mafia(Clone)".Equals(house.GetChild(i).name))
                return true;
        }
        return false;
    }
    
    private bool CheckObsoleteBuild(Transform child)
    {
        SpriteRenderer spriteRenderer = child.GetComponent<SpriteRenderer>();
        string spriteName = spriteRenderer.sprite.name;
        if (spriteName.Contains("желтый дом гг") || spriteName.Contains("зел бол")
                                                 || spriteName.Contains("зел мал")
                                                 || spriteName.Contains("красный маленький ы"))
        {
            return true;
        }
        return false;
    }
}
