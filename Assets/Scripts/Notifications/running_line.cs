﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class running_line : MonoBehaviour
{
    public float speed = 5;
    public InformerScript Info;
    float time = 0f;
    public InfoDate Day;

    public TextAsset data;
    string sms;
    string[] letters;
    public int i;
    Vector3 Srt;
    public void Start()
    {
        data = (TextAsset)Resources.Load("news");
        sms = data.ToString();
        letters = sms.Split('\n');
        this.GetComponent<Text>().text = "";
        Srt = this.transform.localPosition;
    }
    void Update()
    {
        if (this.GetComponent<Text>().text == "" && Day.days % 12 == 0)
        {
            i = Random.Range(4, 13);
            this.GetComponent<Text>().text = letters[i].Remove(0, 7);

        }

        /* if (Info.key == "boont")
         {
             this.GetComponent<Text>().text = GameObject.Find("message").GetComponent<Text>().text;

         }*/

    }
    private void FixedUpdate()
    {
        if (this.GetComponent<Text>().text != "")
        {
            time += 0.02f;
            transform.position = transform.position + new Vector3(speed, 0, 0);
            if (time > 15)
            {
                this.GetComponent<Text>().text = "";
                transform.localPosition = Srt;
                time = 0;
            }
        }
    }
}

