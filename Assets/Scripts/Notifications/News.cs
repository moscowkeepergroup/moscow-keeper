﻿using UnityEngine;

public class News : MonoBehaviour
{
    public GameObject[] objects;

    int numb;
    public string code_word;
    public InformerScript code;
    void Update()
    {
        if (code.code_word == "boont" && !code.active_bottons)
        {
            numb = Random.Range(0, objects.Length - 1);
            Instantiate(objects[numb], objects[numb].transform.position, Quaternion.identity, this.transform);
            code.active_bottons = true;
            code.code_word = "";
            code.key = "boont";
            FindObjectOfType<AudioManager>().Play("News");
            code.pause = true;
        }
    }
}
