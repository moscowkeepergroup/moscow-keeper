﻿// Кнопка открывает панель новостей

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class News_panel : MonoBehaviour
{
    public bool Info = false;
    public Text date;
    public InformerScript ok;
    RectTransform rectt;
    private void Start()
    {
        rectt = this.GetComponent<RectTransform>();
    }
    void Update()
    {
        if (ok.active_bottons)
        {
            this.GetComponent<Text>().enabled = false;
        }
        if (Info)
        {

            this.GetComponent<Text>().text += System.Environment.NewLine + System.Environment.NewLine + date.text + "  " + GameObject.Find("message").GetComponent<Text>().text + System.Environment.NewLine + System.Environment.NewLine;

            Info = !Info;
        }
    }
    public void OnClick()
    {
        FindObjectOfType<AudioManager>().Play("Beep");
        this.GetComponent<Text>().enabled = !this.GetComponent<Text>().enabled;
      
        
    }
  
}
