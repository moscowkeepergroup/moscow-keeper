﻿using UnityEngine;

public class Alarm : MonoBehaviour
{
    public ObjectsScript Objects;
    public GameObject angry;
    public GameObject crowd;
    public Transform point;
    public InformerScript Info;
    bool is_activate;
    public bool button;
    public void Start()
    {
        Info = GameObject.Find("Informer").GetComponent<InformerScript>();
        is_activate = false;
        point = this.transform.GetChild(0);
    }
    public void Update() //появление angry face
    {
        if (button)
        {
            boont();
        }
    }
    public void boont()
    {
        Vector3 pos = transform.position + new Vector3(0, this.GetComponent<population_and_nalogs>().h / 2, 0);
        Instantiate(angry, pos, Quaternion.identity, this.transform);
        Debug.Log("я тута");
        Quaternion rot = this.transform.rotation;
        button = false;
        this.GetComponent<Alarm>().enabled = false;
        if (Info.crowd_count < 3)
        {
            crowd = Instantiate(Objects.crowd, point.position, new Quaternion(rot.eulerAngles.x, -rot.eulerAngles.y, rot.eulerAngles.z, 0f), point.transform);
            if (this.transform.rotation.eulerAngles.y != 0f)
            {
            crowd.transform.Rotate(new Vector3(0, 180, 0));
            }
            Info.crowd_count++;
        }
    }
}
