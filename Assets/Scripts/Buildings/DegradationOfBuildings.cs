﻿using System;
using UnityEngine;
public class DegradationOfBuildings : MonoBehaviour
{
    public ObjectsScript objectsscript;
    public InformerScript _reputation;

    public void GetServiceLife()
    {
        for (int i = 0; i < objectsscript.BUILDINGS.Length; i++)
        {
            objectsscript.BUILDINGS[i].ServiceLife = UnityEngine.Random.Range(1, 3);

        }
        for (int i = 0; i < objectsscript.ROADS.Length; i++)
        {
            objectsscript.ROADS[i].ServiceLife = UnityEngine.Random.Range(1, 6);

        }
    }

    public void MinusMonth()
    {
        for (int i = 0; i < ObjectsScript.Size; i++)
        {
            if (objectsscript.BUILDINGS[i].ServiceLife > 0)
            {
                objectsscript.BUILDINGS[i].ServiceLife--;

            }
        }
        for (int i = 0; i < ObjectsScript.Size2; i++)
        {
            if (objectsscript.ROADS[i].ServiceLife > 0)
            {
                objectsscript.ROADS[i].ServiceLife--;
            }
        }
    }

    // add system of warnings about the oldest building
    // fix counter(number of elements for accidents,buildings,lastrestore)
    public void Start()
    {
        GetServiceLife();                                     // ??? 
    }

    public void Update()
    {
        for (int i = 0; i < objectsscript.BUILDINGS.Length; i++)
        {

            objectsscript.BUILDINGS[i].contentment = _reputation.Reputation + objectsscript.BUILDINGS[i].inviscontentment; ;

            // FOR BUILDINGS
            if (objectsscript.BUILDINGS[i].ServiceLife == 0)
            {

                for (int j = 0; j < 4; j++)
                {
                    if (objectsscript.BUILDINGS[i].objects.GetComponent<SpriteRenderer>().sprite == objectsscript.GetComponent<ObjectsScript>().Newsprites[j])
                    {
                        double X = objectsscript.BUILDINGS[i].objects.transform.position.x;
                        double Y = objectsscript.BUILDINGS[i].objects.transform.position.y;
                        for (int t = 0; t < objectsscript.BUILDINGS.Length; t++)
                        {
                            if ((Math.Abs((decimal)(objectsscript.BUILDINGS[t].objects.transform.position.x - X)) < 3) && (Math.Abs((decimal)(objectsscript.BUILDINGS[t].objects.transform.position.y - Y)) < 3))
                            {
                                if ((objectsscript.BUILDINGS[t].inviscontentment - InformerScript.coefficients.k9) > -100.0)
                                {

                                    objectsscript.BUILDINGS[t].inviscontentment -= InformerScript.coefficients.k9;
                                }
                                else { objectsscript.BUILDINGS[t].inviscontentment = -100; }
                            }

                        }

                        _reputation.Reputation -= InformerScript.coefficients.k1;
                        objectsscript.BUILDINGS[i].objects.GetComponent<SpriteRenderer>().sprite = objectsscript.GetComponent<ObjectsScript>().Oldsprites[j];
                    }
                }
            }
        }
        // FOR ROADS
        for (int i = 0; i < objectsscript.ROADS.Length; i++)
        {
            if (objectsscript.ROADS[i].ServiceLife == 0)
            {
                GameObject Road = GameObject.Find("Objecter").GetComponent<ObjectsScript>().ROADS[i].objects;
                for (int j = 0; j < 3; j++)
                {
                    if (Road.GetComponent<SpriteRenderer>().sprite == GameObject.Find("Objecter").GetComponent<ObjectsScript>().NewspritesRoads[j])
                    {
                        _reputation.Reputation -= InformerScript.coefficients.k2;
                        Road.GetComponent<SpriteRenderer>().sprite = GameObject.Find("Objecter").GetComponent<ObjectsScript>().OldspritesRoads[j];
                    }
                }
            }
        }
        // 1. change authority, anger of people 
        // 2. we need interface elements to restore building in comfortable time
        // 3. we should diferentiate accidents and its influence on buildings condition
        // 4. we need connect restoring building and roads with paying money
    }
}
