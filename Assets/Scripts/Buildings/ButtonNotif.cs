﻿using System.Collections.Generic;
using UnityEngine;

public class ButtonNotif : MonoBehaviour
{
    public static Queue<Date> RestoreTime = new Queue<Date>();
    public static Queue<int> RestoringBuildings = new Queue<int>();
    public static Queue<int> RestoringRoads = new Queue<int>();
    public GameObject ListOfHouses;
    public ObjectsScript Objects;
    public GameObject BuildingReconstruction;
    public GameObject RoadReconstruction;
    public GameObject malo;

    public void DelNotification()
    {
        Destroy(gameObject);
        FindObjectOfType<AudioManager>().Play("Beep");
    }
    public void RestoreNotification() //building menu
    {
        Instantiate(ListOfHouses, ListOfHouses.transform.position, Quaternion.identity);
        FindObjectOfType<AudioManager>().Play("Beep");
    }
    public void RestoreRoadNotification() //состояние дорог
    {
        Instantiate(RoadReconstruction, RoadReconstruction.transform.position, Quaternion.identity);
        FindObjectOfType<AudioManager>().Play("Beep");
        Destroy(gameObject);
    }

    public void RestoreBuildingNotification()
    {
        Instantiate(BuildingReconstruction, BuildingReconstruction.transform.position, Quaternion.identity);
        FindObjectOfType<AudioManager>().Play("Beep");
        Destroy(gameObject);
    }

    public void RestoreOnlyOneElement()
    {
        int a = GetComponentInParent<population_and_nalogs>().numberhouse;

        if (GetComponentInParent<population_and_nalogs>().cost <= GetComponentInParent<population_and_nalogs>().reputation.budget)
        {
            GetComponentInParent<population_and_nalogs>().reputation.budget = GetComponentInParent<population_and_nalogs>().reputation.budget - GetComponentInParent<population_and_nalogs>().cost;
            RestoreTime.Enqueue(GameObject.Find("city-map").GetComponent<SystemOfTime>().GetingTime()); //use parametr
            RestoringBuildings.Enqueue(a);
        }
        else
        {
            Instantiate(malo, transform.position, Quaternion.identity);
        }
        GameObject.Find("Informer").GetComponent<InformerScript>().active_bottons2 = false;
        Destroy(gameObject);
    }

    public void RestoreOnlyOneRoad()
    {
        int a = this.GetComponent<OutputOfconditionals2>().numberoad;
        RestoreTime.Enqueue(GameObject.Find("city-map").GetComponent<SystemOfTime>().GetingTime());//use parametr
        RestoringRoads.Enqueue(a);
        FindObjectOfType<AudioManager>().Play("Drill");
    }
    private void Start()
    {
        Objects = GameObject.Find("Objecter").GetComponent<ObjectsScript>();
    }
    public void RenovationInfrustructure()
    {
        Destroy(gameObject);
        FindObjectOfType<AudioManager>().Play("Beep");

        for (int i = 0; i < Objects.BUILDINGS.Length; i++)
        {
            if ((GameObject.Find("Objecter").GetComponent<ObjectsScript>().BUILDINGS[i].ServiceLife == 1) || (GameObject.Find("Objecter").GetComponent<ObjectsScript>().BUILDINGS[i].ServiceLife == 0))
            {
                RestoreTime.Enqueue(GameObject.Find("city-map").GetComponent<SystemOfTime>().GetingTime());
                RestoringBuildings.Enqueue(i);
                FindObjectOfType<AudioManager>().Play("Beep");
            }
        }
        for (int i = 0; i < Objects.ROADS.Length; i++)
        {
            if ((GameObject.Find("Objecter").GetComponent<ObjectsScript>().ROADS[i].ServiceLife == 1) || (GameObject.Find("Objecter").GetComponent<ObjectsScript>().ROADS[i].ServiceLife == 0))
            {
                Debug.Log("ready");
                RestoreTime.Enqueue(GameObject.Find("city-map").GetComponent<SystemOfTime>().GetingTime());
                RestoringRoads.Enqueue(i);
                FindObjectOfType<AudioManager>().Play("Beep");
            }
        }
    }
}

