
using UnityEngine;
using UnityEngine.UI;

public class addText : MonoBehaviour
{
    public InformerScript X;
    public string[] letters;
    public string[,] variants = new string[4, 5];
    public TextAsset data;
    public string sms;
    public int i;
    public int rand;
    public Text bot1;
    public Text bot2;
    public choice_botton bot;
    void Start()
    {
        data = (TextAsset)Resources.Load("news");
        sms = data.ToString();
        letters = sms.Split('\n');

        for (i = 0; i < 4; i++)
        {
            for (rand = 0; rand < 5; rand++)
            {
                if (letters[i].StartsWith("boont"))
                {
                    letters[i] = letters[i].Remove(0, 6);
                };

                variants[i, rand] = letters[i].Substring(0, letters[i].IndexOf('|'));
                letters[i] = letters[i].Remove(0, letters[i].IndexOf('|') + 1);
            }
        }
    }
    void Update()
    {
        X = GameObject.Find("Informer").GetComponent<InformerScript>();
        if (X.key != "")
        {
            message(X.key, variants);

            Text word = GameObject.Find("News").GetComponent<Text>();
            if (word.text == "")
            {
                word.text = this.GetComponent<Text>().text;
            }
            X.key = "";
            GameObject.Find("news text").GetComponent<News_panel>().Info = true;
        }
    }
    public void message(string x, string[,] text)
    {
        rand = Random.Range(0, rand - 1);
        switch (x)
        {
            case "boont":
                this.GetComponent<Text>().text = text[rand, 0];
                bot1.text = text[rand, 1];
                bot2.text = text[rand, 3];
                bot.p1 = text[rand, 4];
                bot.p2 = text[rand, 2];
                break;
            case "random":
                this.GetComponent<Text>().text = text[i + rand, 0].Remove(0, x.Length + 1);
                break;
        }

    }
}

