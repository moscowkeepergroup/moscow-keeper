﻿using UnityEngine;

public class ObjectsScript : MonoBehaviour
{
    public Camera main_cam;
    public GameObject Node;
    public GameObject human;
    public GameObject button;
    public GameObject crowd;
    public GameObject[] points = new GameObject[4];
    public GameObject Capitol;
    public GameObject boont_menu;
    public const int Size = 21;
    public const int Size2 = 11;
    public const int Size3 = 3;
    [System.Serializable]
    public struct Infrustructure
    {
        public GameObject objects;
        public double inviscontentment;
        public double contentment;
        public int ServiceLife;
    }
    public Infrustructure[] BUILDINGS = new Infrustructure[Size];

    public Infrustructure[] ROADS = new Infrustructure[Size2];
    public Sprite[] Oldsprites = new Sprite[Size3];
    public Sprite[] Newsprites = new Sprite[Size3];
    public Sprite[] OldspritesRoads = new Sprite[Size3];
    public Sprite[] NewspritesRoads = new Sprite[Size3];
}
