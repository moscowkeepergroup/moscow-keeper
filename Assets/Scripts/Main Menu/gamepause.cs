﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gamepause : MonoBehaviour
{
    public InformerScript pause;
    public GameObject menue;
    public GameObject gameEnd;
    public GameObject e;
    public bool aa=false;
    public bool bb = true;
    public GameObject iph;
    public GameObject aaa = null;


    GameObject OO;
    
    
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (pause.pause)
        {
            gamePause();
        }
        if (!pause.pause)
        {
            gamerun();
        }
        if (bb && (pause.budget <= -200000f || pause.Reputation <= -100f || aa))
    
        {
            bb = false;
            End();
        }
    }

    public void gamePause()
    {
        Time.timeScale = 0;
    }
    public void gamerun()
    {
        Time.timeScale = 1;
    }
    public void onclick()
    {
        pause.pause = !pause.pause;
        if (pause.pause == true)
        {
            pause.active_bottons = true;
            pause.active_bottons2 = true;
            OO = Instantiate(menue, transform.position, Quaternion.identity, this.transform);
        }
        else
        {
            pause.active_bottons = false;
            pause.active_bottons2 = false;
            Destroy(OO);
        }
    }
    public void End()
    {
        e = Instantiate(gameEnd, transform.position, Quaternion.identity);
        pause.pause = false;
        if (pause.budget <= -200000)

            e.GetComponentInChildren<Text>().text = "Вы влезли в большие долги, у вас слишком много кредитов, граждане недовольны и президент принял решение снять вас с поста. " + '\n' + "Игра окончена.";

        if (pause.Reputation <= -100)

            e.GetComponentInChildren<Text>().text = "Ваши необдуманные действия, неправильные решения, нежелание вовремя принимать меры всегда ведут только к одному...к провалу. В городе вас очень не любят и путём восстания добились вашей отставки." + '\n' + "Игра окончена";
        if (aa)
            e.GetComponentInChildren<Text>().text = "Что ж. Неумение управлять городом и в особенности полицией привело к тому что вы не смогли подавить массовые восстания и были свергнуты." + '\n' + "Игра окончена.";

    }
    public void iphone()
    {
        
            
            if (aaa == null)
            {
                aaa = Instantiate(iph, transform.position, Quaternion.identity, this.transform);

            }
            else
            {
                Destroy(aaa);


                aaa = null;
            };

        }
    
}
