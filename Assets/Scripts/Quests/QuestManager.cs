﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class QuestManager : MonoBehaviour
{
    private Queue<Quest> _quests;

    private List<Quest> _completedQuests;

    public float shiftTime;

    private bool _isCompleted;

    public InfoDate date;

    public InformerScript info;

    private Text _text;

    //Создает новый квест и записывыает в него все параметры
    private Quest CreateQuest(String descript, params String[] param)
    {
        List<String> curList = new List<String>();
        List<int> targetList = new List<int>();
        int half = param.Length / 2;
        for (int i = 0; i < half; i++)
        {
            curList.Add(param[i]);
            int parseInt;
            if (int.TryParse(param[i + half], out parseInt))
                targetList.Add(parseInt);
            else
                return null;
        }
        var quest = new Quest(descript, curList, targetList);
        quest.SetDescription(descript);
        quest.SetCurrentValues(curList);
        quest.SetTargetValues(targetList);
        return quest;
    }
    //Создает цепочку квестов
    void Start()
    {
        _text = GetComponent<Text>();
        _isCompleted = false;
        _quests = new Queue<Quest>();
        _completedQuests = new List<Quest>();
        _quests.Enqueue(CreateQuest("Собрать 200р",
            "money", "200"));
        _quests.Enqueue(CreateQuest("Собрать 30 полицейских",
            "police", "30"));
        _quests.Enqueue(CreateQuest("Собрать 300р и 40 полицейских",
            "money", "police", "300", "40"));
        _quests.Enqueue(CreateQuest("Продержаться 25 дней", "day", "25"));
        _quests.Enqueue(CreateQuest("Продержаться 1 месяц", "month", "1"));
    }

    //Корутин для перемещения панели квестов вправо перед апдейтом квеста
    private IEnumerator RightShift()
    {
        GameObject parentGameObject = transform.parent.gameObject;
        Vector3 parentPosition = parentGameObject.transform.position;

        float x = parentPosition.x;
        float y = parentPosition.y;
        float z = parentPosition.z;
        for (float i = 0; i < 10; i++)
        {
            parentGameObject.transform.position = new Vector3(x + i, y, z);
            yield return new WaitForSeconds(shiftTime / 20);
        }
        //        _completedQuests.Add(_quests.Dequeue());
        CompletingQuest();
    }

    //Корутин для перемещения панели квестов влево после апдейта квеста
    private IEnumerator LeftShift()
    {
        GameObject parentGameObject = transform.parent.gameObject;
        Vector3 parentPosition = parentGameObject.transform.position;

        float x = parentPosition.x;
        float y = parentPosition.y;
        float z = parentPosition.z;
        for (float i = 0; i < 10; i++)
        {
            parentGameObject.transform.position = new Vector3(x - i, y, z);
            yield return new WaitForSeconds(shiftTime / 20);
        }
    }

    // Update is called once per frame
    private void CompletingQuest()
    {
        _completedQuests.Add(_quests.Dequeue());
        _isCompleted = false;
        StartCoroutine(LeftShift());
    }

    void Update()
    {
        if (_quests.Count == 0) return; //Проверяет что цепочка квестов не пустая
        _text.text = ShowStats(); //Выводит строку текущего квеста
        if (CheckComplete() && !_isCompleted) //Проверяет квест на выполнение, если квест выполнен удаляет из очереди
        {
            _isCompleted = true;
            StartCoroutine(RightShift());
            Debug.Log("Quest completed!!!");
        }
    }

    //Возвращает параметр, в зависимости от квеста
    private int GetNum(String str)
    {
        if (str.ToLower().Equals("money"))
            return (int)info.budget;
        if (str.ToLower().Equals("police"))
            return info.police_count;
        if (str.ToLower().Equals("day"))
            return (int)date.days;
        if (str.ToLower().Equals("month"))
            return (int)date.months + ((int)date.years - 2019) * 12 - 8;
        if (str.ToLower().Equals("year"))
            return (int)date.years - 2019;
        return 0;
    }

    //Создает строку для вывода в UI
    public String ShowStats()
    {
        var res = "Задание:\n" + _quests.Peek().GetDescription() + "\n";
        res += "Получено:\n";
        List<String> currentValues = _quests.Peek().GetCurrentValues();
        List<int> targetValues = _quests.Peek().GetTargetValues();
        for (int i = 0; i < currentValues.Count; i++)
        {
            int num;
            num = GetNum(currentValues.ElementAt(i));
            res += num + " / " + targetValues.ElementAt(i) + "\n";
        }
        return res;
    }

    //Проверяет текущий квест на выполнение
    public bool CheckComplete()
    {
        List<String> currentValues = _quests.Peek().GetCurrentValues();
        List<int> targetValues = _quests.Peek().GetTargetValues();

        for (int i = 0; i < currentValues.Count; i++)
        {
            if (GetNum(currentValues.ElementAt(i)) < targetValues.ElementAt(i))
                return false;
        }
        return true;
    }
}
