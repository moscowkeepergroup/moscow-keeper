﻿using UnityEngine;
using Pathfinding;

public class parent : MonoBehaviour
{
    GameObject p;
    public GameObject n;

    //public InformerScript Info;
    void Start()
    {
        p = transform.parent.gameObject;
    }
    public void antiboont()
    {
        if (GameObject.Find("Informer").GetComponent<InformerScript>().police_count >= 1)
        {
            GameObject.Find("Informer").GetComponent<InformerScript>().police_count--;
            GameObject.Find("Informer").GetComponent<InformerScript>().active_bottons2 = false;
            FindObjectOfType<AudioManager>().Play("Punch");
            Destroy(p);
        }
        else
        {
            Instantiate(n, transform.position, Quaternion.identity, this.transform);
        }
    }
}
