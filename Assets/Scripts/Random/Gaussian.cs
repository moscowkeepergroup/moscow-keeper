﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class Gaussian : MonoBehaviour
{
	public static float NextGaussian(float mean, float stdDev)
	{
		Random rand = new Random();
		double uniform1, uniform2, randStdNorm, randGaussian;
		
		uniform1 = 1.0-rand.NextDouble();
		uniform2 = 1.0-rand.NextDouble();

		randStdNorm = Math.Sqrt(-2.0 * Math.Log(uniform1)) *
		              Math.Sin(2.0 * Math.PI * uniform2);
		randGaussian =
			mean + stdDev * randStdNorm;
		return (float) randGaussian;
	}  
}
