﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class PoliceAnim : MonoBehaviour
{
    const float delta = 0.05f;
    public AIPath ai;
    public Vector2 velocity;
    public int direction = -1;
    public Animator animator;
    public AIDestinationSetter target;
    public GameObject a;
    private void ChangeAnim()
    {
        try
        {
            animator.SetInteger("Direction", direction);
        }
        catch (System.Exception)
        {
            ;
        }
    }

    void UpdateDir()
    {
        if (velocity.x > 0 - delta && velocity.y > 0 + delta)//
            direction = 0;
        else if (velocity.x > 0 + delta && velocity.y < 0 + delta)//
            direction = 1;
        else if (velocity.x < 0 + delta && velocity.y < 0 - delta)//
            direction = 2;
        else if (velocity.y > 0 - delta && velocity.x < 0 - delta)
            direction = 3;
        else direction = -1;
    }
    private void Start()
    {
        direction = -1;
        animator = this.GetComponent<Animator>();
        ai = this.GetComponent<AIPath>();
        velocity = ai.velocity;
    }
    // Update is called once per frame
    void Update()
    {
        velocity = ai.velocity;
        UpdateDir();
        ChangeAnim();

    }
}